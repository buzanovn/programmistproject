<!DOCTYPE html>
<html lang="ru">
<head>
    <title> Программистский проект </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Кодировка страницы -->
    <meta charset="utf-8">
    <!-- Основной Bootstrap-стиль   -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
    <!-- Собственный стиль оформления -->
    <link href="assets/css/style.css" rel="stylesheet"/>
    <!-- Шрифт Open Sans от Google Fonts -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/fonts.css">
    <script src="assets/javascript/jquery.js"></script>
    <script src="assets/javascript/bootstrap.min.js"></script>
</head>
<body>
<?php
    $error = $_REQUEST['error'];
?>
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" method="POST" action="api/session.php?action=initiate">
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="login" name="login" class="form-control" placeholder="Логин" required autofocus>
            <?php if (isset($error) && $error == "login") { echo "Error!"; } ?>
            <input type="password" id="password" name="password" class="form-control" placeholder="Пароль" required>
            <?php if (isset($error) && $error == "password") { echo "Error!"; } ?>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Войти</button>
        </form>
    </div>
</div>
</body>
</html>
