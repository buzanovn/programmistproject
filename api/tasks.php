<?php
if (isset($_REQUEST['action'])) {
    switch ($_REQUEST['action']) {
        case "delete":
            $id = $_REQUEST['id'];
            $result = $connector->delete_task($id);
            if ($result) {
                header('Location: index.php?operation_status=success');
            } else {
                header('Location: index.php?operation_status=error');
            }
            break;
        case "update":
            $id = $_REQUEST['id'];
            $name = $_REQUEST['name'];
            $diff = $_REQUEST['diff'];
            $status = $_REQUEST['status'];
            break;
        case "assign":
            $id = $_REQUEST['id'];
            $login = $_SESSION['login'];
            $connector->assign_task($id, $login);
    }
}
?>