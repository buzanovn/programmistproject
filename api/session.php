<?php
require_once '../classes/AuthorizationValidator.php';
echo "Got here";
if(isset($_REQUEST['action'])) {
    switch($_REQUEST['action']) {
        case "initiate":
            echo "Initiated";
            $login = $password = "";
            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $login = $_POST["login"];
                $password = $_POST["password"];
                $validator = new AuthorizationValidator();
                $validation_result = $validator->validate($login, $password);
                switch ($validation_result) {
                    case AuthorizationValidator::$ALL_VALID:
                        header('Location: index.php');
                        break;
                    case AuthorizationValidator::$LOGIN_NOT_VALID:
                        header('Location: authorize.php?error=login');
                        break;
                    case AuthorizationValidator::$PASSWORD_NOT_VALID:
                        header('Location: authorize.php?error=password');
                }
            }
            break;
        case "terminate":
            session_start();
            session_destroy();
            header('Location: authorize.php');
            break;
    }
}
?>