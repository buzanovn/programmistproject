<!DOCTYPE html>
<html lang="ru">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <title> Программистский проект </title>

    <link href="assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/fonts.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet"/>

    <script src="assets/javascript/jquery.js"></script>
    <script src="assets/javascript/bootstrap.min.js"></script>

    <script src="./assets/javascript/fix_tabs_switching.js" type="text/javascript"></script>
</head>
<body>
<?php
session_start();
if (!isset($_SESSION['login'])) {
    header('Location: authorize.php?error=unauthorized_access');
}
?>
<nav class="navbar-inverse">
    <div class="container-fluid">
        <ul id="tabs" data-tabs="tabs" class="nav navbar-nav nav-tabs">
            <li class="active">
                <a href="#tasks_tab">
                    <div class="tab_name">
                        <i class="fa fa-tasks" aria-hidden="true"></i> Задачи
                    </div>
                </a>
            </li>
            <li>
                <a href="#programmist_tab">
                    <div class="tab_name">
                        <i class="fa fa-user" aria-hidden="true"></i> Программист
                    </div>
                </a>
            </li>
        </ul>

        <ul class="nav navbar-nav navbar-right">
            <li>
                <a href="#">
                    <div class="tab_name">
                        <i class="fa fa-upload"></i> Загрузить
                    </div>
                </a>
            </li>
            <li>

            </li>
            <li class="dropdown">
                <a class="dropdown-toggle" role="button" data-toggle="dropdown">
                    <div class="tab_name">
                        <i class="fa fa-user"></i> <?php echo $_SESSION['login'] ?> <span class="caret"></span>
                    </div>
                </a>
                <ul class="dropdown-menu">
                    <li><a href="api/session.php?action=terminate" type="submit">
                            <div class="tab_name">
                                <i class="fa fa-lock"></i> Выход
                            </div>
                        </a></li>
                </ul>
    </div>
</nav>
<div class="tab-content">
    <div id="tasks_tab" data-toggle="tab" class="tab-pane fade in active">
        <h3> Задачи </h3>
        <?php include 'test.php' ?>
    </div>
    <div id="programmist_tab" data-toggle="tab" class="tab-pane fade">
        <div class="panel-group" id="programmist_acc">
            <div class="panel">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#programmist_acc" href="#collapse1">Информация о
                            программисте</a>
                    </h4>
                    <div>
                        <div class="card card-container"></div>
                        <div id="collapse1" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <?php include 'get_users.php' ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
