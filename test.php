<?php require_once("classes/MySqliConnector.php");
require_once("classes/TaskTableFormatter.php");
    $connector = new MySqliConnector;
    $result = $connector->query("SELECT name, difficulty, status
FROM Task WHERE idTask IN (
  SELECT Task_idTask
  FROM TaskProgrammer
  WHERE Programmer_idProgrammer IN (
    SELECT idProgrammer
    FROM Programmer
    WHERE login = \"buzanovn\"
  )
)");
    $array = new ArrayObject();
    while($nv = $result->fetch_assoc()) {
        $array->append($nv);
    }
    $formatter = new TaskTableFormatter();
    echo $formatter->build($array);
    ?>