<?php

class TaskTableFormatter
{
    private $NAME = "Название задачи";
    private $DIFFICULTY = "Сложность";
    private $STATUS = "Статус выполнения";

    public function table($inner) {
        return "<table class=\"table table-bordered task-table\">" . $inner . "</table>";
    }

    public function tr($inner) {
        return "<tr>" . $inner . "</tr>";
    }

    public function td($inner) {
        return "<td>" . $inner . "</td>";
    }

    public function th($inner) {
        return "<th>" . $inner . "</th>";
    }

    public function row($name, $diff, $status) {
        return $this->tr($this->td($name) . $this->td($this->format_difficulty($diff)) . $this->td($status));
    }

    public function title() {
        return $this->tr($this->th($this->NAME) . $this->th($this->DIFFICULTY) . $this->th($this->STATUS));
    }

    public function format_difficulty($diff) {
        $result = "";
        $icon = "<i class=\"fa fa-exclamation-triangle fa-$diff\"></i> ";
        for ($i = 0; $i < $diff; $i++) {
            $result .= $icon;
        }
        return $result;
    }

    public function build($array) {
        $result = "";
        $result .= $this->title();
        foreach ($array as &$row) {
            $result .= $this->row($row['name'], $row['difficulty'], $row['status']);
        }
        return $this->table($result);
    }
}