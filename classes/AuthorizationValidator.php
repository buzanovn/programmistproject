<?php
require_once('MySqliConnector.php');

class AuthorizationValidator
{
    private $connector = null;
    public static $LOGIN_NOT_VALID = "LNV";
    public static $PASSWORD_NOT_VALID = "PNV";
    public static $ALL_VALID = "V";


    public function __construct()
    {
        $this->connector = new MySqliConnector();
    }

    public function validate($login, $password) {
        $login_result = $this->connector->check_user_exists($login);
        if ($login_result) {
            $password_result = $this->connector->check_password_correct($login, $password);
            if ($password_result) {
                session_start();
                $_SESSION['login'] = $login;
                $_SESSION['password'] = $password;
                return AuthorizationValidator::$ALL_VALID;
            } else {
                return AuthorizationValidator::$PASSWORD_NOT_VALID;
            }
        } else {
            return AuthorizationValidator::$LOGIN_NOT_VALID;
        }
    }
}