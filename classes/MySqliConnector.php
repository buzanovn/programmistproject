<?php

/**
 * Created by PhpStorm.
 * User: guest
 * Date: 5/22/17
 * Time: 2:25 AM
 */
class MySqliConnector extends mysqli
{
    private static $HOST = "localhost";
    private static $USER = "root";
    private static $PASSWD = "1608";
    private static $NAME = "ProgrammersProject";

    public function __construct()
    {
        parent::__construct(MySqliConnector::$HOST, MySqliConnector::$USER, MySqliConnector::$PASSWD, MySqliConnector::$NAME);
        if (mysqli_connect_error()) {
            exit('Connect Error (' . mysqli_connect_errno() . ') ' . mysqli_connect_error());
        }
        parent::set_charset('utf8');
        parent::query("SET lc_time_names = 'ru_RU'");
    }

    public function select_users() {
        return $this->query("SELECT idProgrammer, credentials, login FROM ProgrammersProject.Programmer;");
    }

    public function select_by_login($login) {
        return $this->query("SELECT idProgrammer, credentials, login FROM ProgrammersProject.Programmer WHERE login=\"$login\";");
    }

    public function check_user_exists($login) {
        $result = $this->query("SELECT Programmer_login FROM ProgrammersProject.Authorization WHERE Programmer_login=\"$login\";");
        if ($result->num_rows == 1) {
            return true;
        } else if ($result->num_rows == 0) {
            return false;
        } else {
            return false;
        }
    }

    public function check_password_correct($login ,$password) {
        $result = $this->query("SELECT password FROM ProgrammersProject.Authorization WHERE Programmer_login=\"$login\"");
        if($result->num_rows == 1) {
            $db_password = $result->fetch_assoc();
            if ($db_password['password'] == $password) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function delete_task($id) {
        $result = $this->query("SELECT * FROM ProgrammersProject.Task WHERE idTask=\"$id\"");
        if($result->num_rows > 0) {
            $this->query("DELETE FROM Task WHERE idTask=\"$id\"");
            return true;
        } else {
            return false;
        }
    }

    public function select_tasks($login) {

    }

    public function assign_task($id, $login)
    {
        $result = $this->query("SELECT * FROM ProgrammersProject.Task WHERE idTask=\"$id\"");
        if($result->num_rows > 0) {
            $idlresult = $this->query("SELECT idProgrammer FROM Programmer WHERE login=\"$login\"");
            if ($idlresult->num_rows > 0) {
                $idp = $idlresult->fetch_assoc();
                $idp = $idp['idProgrammer'];
                $this->query("INSERT INTO TaskProgrammer VALUES(NULL, $id, $idp);");
            }
        }
    }


}